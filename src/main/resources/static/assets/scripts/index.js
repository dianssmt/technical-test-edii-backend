$(document).ready(function () {
	console.log('test');
});

$('#save').click(function () {
	console.log("tombol save");
	formBook.saveForm();
	formBook.resetForm();
})

var formBook = {
	resetForm: function () {
		$('#formBooking')[0].reset();
	},

	saveForm: function () {
		var data = getJsonForm($('#formBooking').serializeArray(), true);
		$.ajax({
			url: '/booking',
			method: 'post',
			contentType: 'application/json',
			dataType: 'json',
			data: JSON.stringify(data),
			success: function (result) {
				tableBook.getData();
				console.log(result);
			},
			error: function (error) {
				console.log(error)
			}
		})
	}
}

var tableBook = {
	getData: function () {
	console.log("get")
		$.ajax({
			url: '/booking',
			method: 'get',
			data: {
				description: $('#description').val(),
			},
			success: function (result) {
				$('#tbodyBooking').html('')
				$.each(result.data, function (index, book) {
					$('#tbodyBooking').append(
						"<tr>" +
						"<td>" + book.idRequestBooking + "</td>" +
						"<td>" + book.description + "</td>" +
						"</tr>"
					)
				})
			},
			error: function (error) {
				console.log(error);
			}
		})
	}
}