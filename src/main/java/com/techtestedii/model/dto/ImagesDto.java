package com.techtestedii.model.dto;

import lombok.Data;

@Data
public class ImagesDto {
	private String idRequestBooking;
	private String description;
}
