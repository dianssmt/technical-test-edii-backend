package com.techtestedii.model.dto;

import lombok.Data;

@Data
public class LogDto {
	private String idRequestBooking;
	private String idPlatform;
	private String namaPlatform;
	private String docType;
	private String termOfPayment;
}
