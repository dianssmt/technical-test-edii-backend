package com.techtestedii.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "images_nle_api01_dian")
public class ImagesEntity {
	@Id
	@Column(name = "idRequestBooking", nullable = false)
	private String idRequestBooking;

	@Column(name = "description")
	private String description;
	
}
