package com.techtestedii.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "log_nle_api01_dian")
public class LogEntity {
	@Id
	@Column(name = "idRequestBooking", nullable = false)
	private String idRequestBooking;

	@Column(name = "id_platform", length = 20)
	private String idPlatform;
	
	@Column(name = "nama_platform", length = 20)
	private String namaPlatform;
	
	@Column(name= "doc_type", length = 20)
	private String docType;
	
	@Column(name= "term_of_payment", length = 5)
	private String termOfPayment;

}
