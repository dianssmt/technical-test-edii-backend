package com.techtestedii.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techtestedii.configuration.DefaultResponse;
import com.techtestedii.model.dto.LogDto;
import com.techtestedii.model.entity.LogEntity;
import com.techtestedii.repository.LogRepository;
import com.techtestedii.service.LogService;

@RestController
@RequestMapping("/log")
@CrossOrigin(origins = "http://localhost:3000")
public class LogController {

	@Autowired
	private LogRepository repository;
	
	@Autowired
	private LogService service;
	
	@PostMapping
	public DefaultResponse insert(@RequestBody LogDto dto) {
		return DefaultResponse.ok(service.insert(dto));
	}
	
	@GetMapping
	public DefaultResponse get() {
		List<LogEntity> imagesList = repository.findAll();
		List<LogDto> imagesDtoList = imagesList.stream().map(images -> convertToDto(images))
				.collect(Collectors.toList());
		return DefaultResponse.ok(imagesDtoList);
	}
	
	@GetMapping("/{id}")
	public DefaultResponse getById(@PathVariable String id) {
		LogEntity entity = repository.findById(id).get();
		LogDto dto = convertToDto(entity);
		return DefaultResponse.ok(dto);
	}
	
	private LogDto convertToDto(LogEntity entity) {
		LogDto dto = new LogDto();
		dto.setIdRequestBooking(entity.getIdRequestBooking());
		dto.setIdPlatform(entity.getIdPlatform());
		dto.setNamaPlatform(entity.getNamaPlatform());
		dto.setDocType(entity.getDocType());
		dto.setTermOfPayment(entity.getTermOfPayment());
		return dto;
	}
	
	@PutMapping("/{id}")
	public DefaultResponse update(@PathVariable String id, @RequestBody LogDto entity) {
		return DefaultResponse.ok(service.update(id, entity));
	}
	
	@DeleteMapping("/{id}")
	public DefaultResponse delete(@PathVariable String id) {
		return DefaultResponse.ok(service.delete(id));
	}
}
