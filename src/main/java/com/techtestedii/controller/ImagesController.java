package com.techtestedii.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techtestedii.configuration.DefaultResponse;
import com.techtestedii.model.dto.ImagesDto;
import com.techtestedii.model.entity.ImagesEntity;
import com.techtestedii.model.entity.LogEntity;
import com.techtestedii.repository.ImagesRepository;
import com.techtestedii.service.ImagesService;

@RestController
@RequestMapping("/images")
@CrossOrigin(origins = "http://localhost:3000")
public class ImagesController {

	@Autowired
	private ImagesRepository repository;
	
	@Autowired
	private ImagesService service;
	
	@PostMapping
	public DefaultResponse insert(@RequestBody ImagesDto dto) {
		return DefaultResponse.ok(service.insert(dto));
	}
	
	@GetMapping
	public DefaultResponse get() {
		List<ImagesEntity> imagesList = repository.findAll();
		List<ImagesDto> imagesDtoList = imagesList.stream().map(images -> convertToDto(images))
				.collect(Collectors.toList());
		return DefaultResponse.ok(imagesDtoList);
	}
	
	@GetMapping("/{id}")
	public DefaultResponse getById(@PathVariable String id) {
		ImagesEntity entity = repository.findById(id).get();
		ImagesDto dto = convertToDto(entity);
		return DefaultResponse.ok(dto);
	}
	
	private ImagesDto convertToDto(ImagesEntity entity) {
		ImagesDto dto = new ImagesDto();
		dto.setIdRequestBooking(entity.getIdRequestBooking());
		dto.setDescription(entity.getDescription());
		return dto;
	}
	
	@PutMapping("/{id}")
	public DefaultResponse update(@PathVariable String id, @RequestBody ImagesDto entity) {
		return DefaultResponse.ok(service.update(id, entity));
	}
	
	@DeleteMapping("/{id}")
	public DefaultResponse delete(@PathVariable String id) {
		return DefaultResponse.ok(service.delete(id));
	}
}
