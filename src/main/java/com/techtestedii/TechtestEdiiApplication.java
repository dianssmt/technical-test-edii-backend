package com.techtestedii;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechtestEdiiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechtestEdiiApplication.class, args);
	}

}
