package com.techtestedii.service;

import com.techtestedii.model.dto.ImagesDto;
import com.techtestedii.model.entity.ImagesEntity;

public interface ImagesService {

	ImagesDto insert(ImagesDto dto);

	ImagesEntity update(String id, ImagesDto newBooking);

	ImagesEntity delete(String id);

}