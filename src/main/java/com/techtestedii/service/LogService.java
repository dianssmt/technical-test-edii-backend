package com.techtestedii.service;

import com.techtestedii.model.dto.LogDto;
import com.techtestedii.model.entity.LogEntity;

public interface LogService {

	LogDto insert(LogDto dto);

	LogEntity update(String id, LogDto newBooking);

	LogEntity delete(String id);


}