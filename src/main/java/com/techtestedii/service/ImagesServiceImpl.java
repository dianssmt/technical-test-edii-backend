package com.techtestedii.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techtestedii.model.dto.ImagesDto;
import com.techtestedii.model.entity.ImagesEntity;
import com.techtestedii.repository.ImagesRepository;

@Service
@Transactional
public class ImagesServiceImpl implements ImagesService {

	@Autowired
	private ImagesRepository repository;

	@Override
	public ImagesDto insert(ImagesDto dto) {
		ImagesEntity entity = convertToEntity(dto);
		repository.save(entity);
		return dto;
	}

	@Override
	public ImagesEntity update(String id, ImagesDto newBooking) {
		ImagesEntity oldBooking = repository.findById(id).get();
		if (newBooking.getIdRequestBooking() != null)
			oldBooking.setIdRequestBooking(newBooking.getIdRequestBooking());
		if (newBooking.getDescription() != null)
			oldBooking.setDescription(newBooking.getDescription());
		final ImagesEntity update = repository.save(oldBooking);
		return update;
	}

	@Override
	public ImagesEntity delete(String id) {
		ImagesEntity entity = new ImagesEntity();
		if (repository.findById(id).isPresent()) {
			entity = repository.findById(id).get();
			repository.delete(entity);
		}
		return entity;
	}

	private ImagesEntity convertToEntity(ImagesDto dto) {
		ImagesEntity entity = new ImagesEntity();
		entity.setIdRequestBooking(dto.getIdRequestBooking());
		entity.setDescription(dto.getDescription());
		return entity;
	}
}

