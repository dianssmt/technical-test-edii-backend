package com.techtestedii.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techtestedii.model.dto.LogDto;
import com.techtestedii.model.dto.LogDto;
import com.techtestedii.model.entity.LogEntity;
import com.techtestedii.model.entity.LogEntity;
import com.techtestedii.repository.LogRepository;
import com.techtestedii.repository.LogRepository;

@Service
@Transactional
public class LogServiceImpl implements LogService {

	@Autowired
	private LogRepository repository;

	@Override
	public LogDto insert(LogDto dto) {
		LogEntity entity = convertToEntity(dto);
		repository.save(entity);
		return dto;
	}

	@Override
	public LogEntity update(String id, LogDto newLog) {
		LogEntity oldLog = repository.findById(id).get();
		if (newLog.getIdRequestBooking() != null)
			oldLog.setIdRequestBooking(newLog.getIdRequestBooking());
		if (newLog.getIdPlatform() != null)
			oldLog.setIdPlatform(newLog.getIdPlatform());
		if(newLog.getNamaPlatform()!=null)
			oldLog.setNamaPlatform(newLog.getNamaPlatform());
		if(newLog.getDocType()!=null)
			oldLog.setDocType(newLog.getDocType());
		if(newLog.getTermOfPayment()!=null)
			oldLog.setTermOfPayment(newLog.getTermOfPayment());
		final LogEntity update = repository.save(oldLog);
		return update;
	}

	@Override
	public LogEntity delete(String id) {
		LogEntity entity = new LogEntity();
		if (repository.findById(id).isPresent()) {
			entity = repository.findById(id).get();
			repository.delete(entity);
		}
		return entity;
	}

	private LogEntity convertToEntity(LogDto dto) {
		LogEntity entity = new LogEntity();
		entity.setIdRequestBooking(dto.getIdRequestBooking());
		entity.setIdPlatform(dto.getIdPlatform());
		entity.setNamaPlatform(dto.getNamaPlatform());
		entity.setDocType(dto.getDocType());
		entity.setTermOfPayment(dto.getTermOfPayment());
		return entity;
	}
}

