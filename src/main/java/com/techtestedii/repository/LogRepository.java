package com.techtestedii.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techtestedii.model.entity.LogEntity;


@Repository
public interface LogRepository extends JpaRepository<LogEntity, String>{
	
}