package com.techtestedii.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techtestedii.model.entity.ImagesEntity;


@Repository
public interface ImagesRepository extends JpaRepository<ImagesEntity, String>{
	
}